package com.example.teatimer

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.EditText
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import android.R.attr.button
import android.R.attr.countDown
import android.content.Context
import android.content.Intent
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.view.WindowManager
import android.widget.Button


class MainActivity : AppCompatActivity() {

    lateinit var teaTimer : TeaTimer
    private var audio :Audio = Audio(this)
    private var isTimerOn : Boolean = false;
    lateinit var clockTextView: TextView
    lateinit var countDownTimer: CountDownTimer
    var context: Context = this


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        teaTimer = TeaTimer()
        clockTextView = findViewById(R.id.clockTextView)


        cancel_id.setOnClickListener ({ clickCancel() })

        brew_black_id.setOnClickListener ({ clickBrewBlack() })

        brew_green_id.setOnClickListener({clickBrewGreen()})

        brew_white_id.setOnClickListener({ clickBrewWhite() })

        brew_custom_id.setOnClickListener({ clickBrewCustom() })

    }
    private fun clickCancel(){
        if(!isTimerOn) {
            return
        }
        countDownTimer.cancel()
        isTimerOn = false
        clockTextView.text = "done"
        window.clearFlags((WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON))

    }

    private fun clickBrewBlack(){
         if(isTimerOn) {
             return
         }
             val time: Int = teaTimer.getRandomTimeInMillis(3, 5)
             startTimer(time)

    }
    private fun clickBrewGreen(){


        if(isTimerOn) {
            return
        }

           val time: Int = teaTimer.getRandomTimeInMillis(2, 3)
            startTimer(time);

    }
    fun clickBrewWhite(){

        if(isTimerOn) {
            return
        }
            val time: Int = teaTimer.getRandomTimeInMillis(1, 3)
            startTimer(time);


    }
    fun clickBrewCustom(){
        if(isTimerOn) {
        return
        }
            var from = findViewById<EditText>(R.id.from_id).text.toString().toInt();
            var to = findViewById<EditText>(R.id.to_id).text.toString().toInt();
            if (from >= to) {
                print("From cannot be higher or equal to To")
            } else {
                val time: Int = teaTimer.getRandomTimeInMillis(from, to);
                startTimer(time);
        }
    }

    fun startTimer( timeMillis: Int){
        isTimerOn =true;
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        countDownTimer = object: CountDownTimer(timeMillis.toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                clockTextView.text = "seconds remaining: " + millisUntilFinished / 1000
            }
            override fun onFinish() {
                window.clearFlags((WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON))
                clockTextView.text = "done"
                isTimerOn =false;
                val intent = Intent(context, AlarmActivity::class.java)
                startActivity(intent);
            }
        }.start()
    }


}
