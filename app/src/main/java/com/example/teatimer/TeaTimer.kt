package com.example.teatimer

import kotlin.random.Random

class TeaTimer {


    fun getRandomTimeInMillis(from: Int, to: Int): Int {
        return (Random.nextDouble(from.toDouble(), to.toDouble()) * 60000).toInt()
    }

}


