package com.example.teatimer

import android.content.Context
import android.media.MediaPlayer
import android.provider.MediaStore

class Audio(context: Context) {
    private  var mediaPlayer: MediaPlayer = MediaPlayer()
    val context : Context = context

    fun play(sound: Int, loop: Boolean) {
        if(!mediaPlayer.isPlaying) {
            mediaPlayer = MediaPlayer.create(context, sound)
            mediaPlayer.isLooping = loop
            mediaPlayer.start()
        }
    }

    fun stop() {
        if (mediaPlayer.isPlaying) {
            mediaPlayer.stop()
            mediaPlayer.release()
        }
    }
}